package utils

import org.gradle.api.Project
import java.util.*

/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:43.
 * ===================================================
 * Package              : utils
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */


private const val LOCAL_PROPERTIES_FILE_NAME = "local.properties"
/**
 * Util to obtain property declared on `$projectRoot/local.properties` file.
 *
 * @param propertyName the name of declared property
 * @param project the project reference
 *
 * @return the value of property name, otherwise throw [Exception]
 */
fun getLocalProperty(propertyName: String, project: Project): String {
    val localProperties = Properties().apply {
        val localPropertiesFile = project.rootProject.file(LOCAL_PROPERTIES_FILE_NAME)
        if (localPropertiesFile.exists()) {
            load(localPropertiesFile.inputStream())
        }
    }

    return localProperties.getProperty(propertyName)?.let {
        it
    } ?: run {
        throw NoSuchFieldException("Not defined property: $propertyName")
    }
}