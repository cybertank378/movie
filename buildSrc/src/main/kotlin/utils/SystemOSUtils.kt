package utils

import java.util.*

/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:45.
 * ===================================================
 * Package              : utils
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
/**
 * Util to check if the project run on Linux or Mac operating system
 *
 * @return true if the operating system is one of them
 */
fun isLinuxOrMacOs(): Boolean {
    val osName = System.getProperty("os.name").toLowerCase(Locale.ROOT)
    return listOf("linux", "windows", "macos").contains(osName)
}