/**
 * Created              : Rahman on 15/07/2022.
 * Date Created         : 15/07/2022 / 19:24.
 * ===================================================
 * Package              :
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
object BuildDependenciesVersions {
    // Gradle Plugin
    const val  gradleVersion = "7.2.1"
    const val  hiltGradle  = "2.42"
    const val navGradle = "2.5.0"
    // Core
    const val kotlinVersion = "1.7.0"
    const val appCompatVersion = "1.2.0"
    const val coreKtxVersion = "1.8.0"
    const val navVersion = "2.5.0"

    // Background
    const val coroutinesVersion = "1.5.0"
    
    // Design
    const val materialVersion = "1.6.1"
    const val recyclerviewVersion = "1.2.1"
    const val constraintVersion = "2.1.4"
    
    // Hilt
    const val hiltVersion = "2.42"
    const val hiltLifecycleVersion = "1.0.0-alpha03"
    const val hiltCompilerVersion = "1.0.0"
    const val hiltWorkVersion = "1.0.0"
    const val workVersion = "2.7.1"
    // Livecycle
    const val lifecycleVersion = "2.5.0"
    const val lifecycleExtVersion = "2.2.0"
    //Spotless
    const val spotlessVersion = "6.8.0"

    //Network
    const val retrofitVersion = "2.9.0"
    const val okhttp3Version = "4.10.0"
    const val glideVersion = "4.11.0"
    const val timberVersion = "4.7.1"

    //Inject
    const val javaxInjectVersion = "1"

    //Test
    const val junitVersion = "4.13.2"
    const val junitExtVersion = "1.1.3"
    const val espressoVersion = "3.4.0"
}