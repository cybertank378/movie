/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:13.
 * ===================================================
 * Package              :
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
object BuildTasksGroups {
    const val ANDROID = "android"
    const val BUILD = "build"
    const val BUILD_SETUP = "build setup"
    const val CLEANUP = "cleanup"
    const val DOCUMENTATION = "documentation"
    const val FORMATTING = "formatting"
    const val GIT_HOOKS = "git hooks"
    const val HELP = "help"
    const val INSTALL = "install"
    const val OTHER = "other"
    const val REPORTING = "reporting"
    const val VERIFICATION = "verification"
}