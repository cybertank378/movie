/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:14.
 * ===================================================
 * Package              :
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
object BuildPlugins {
    const val ANDROID_APPLICATION = "com.android.application"
    const val KOTLIN_ANDROID = "org.jetbrains.kotlin.android"
    const val KOTLIN_PARCELIZE = "kotlin-parcelize"
    const val HILT_DAGGER = "dagger.hilt.android.plugin"
    const val KOTLIN_KAPT = "kotlin-kapt"

    const val NAVIGATION_SAFE_ARGS = "androidx.navigation.safeargs.kotlin"

    const val SPOTLESS = "plugins.spotless"
    const val UPDATE_DEPENDENCIES = "plugins.update-dependencies"
}