
import java.io.File
import java.io.FileInputStream
import java.util.*

/**
 * Created              : Rahman on 15/07/2022.
 * Date Created         : 15/07/2022 / 19:50.
 * ===================================================
 * Package              :
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */

object KeyHelper {
    private val properties by lazy {
        Properties().apply { load(FileInputStream(File("gradle.properties"))) }
    }

    fun getValue(key: String): String {
        return properties.getProperty(key)
    }
}