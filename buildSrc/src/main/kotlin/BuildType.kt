/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:52.
 * ===================================================
 * Package              :
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
interface BuildType {

    companion object {
        const val DEBUG = "debug"
        const val RELEASE = "release"
    }

    val isMinifyEnabled: Boolean
    val isTestCoverageEnabled: Boolean
}

object BuildTypeDebug : BuildType {
    override val isMinifyEnabled = false
    override val isTestCoverageEnabled = true

    const val applicationIdSuffix = ".debug"
    const val versionNameSuffix = "-DEBUG"
}

object BuildTypeRelease : BuildType {
    override val isMinifyEnabled = true
    override val isTestCoverageEnabled = false
}