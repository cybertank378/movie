package extensions

import org.gradle.api.Project
import utils.getLocalProperty

/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:41.
 * ===================================================
 * Package              : extensions
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
/**
 * Obtain property declared on `$projectRoot/local.properties` file.
 *
 * @param propertyName the name of declared property
 */
fun Project.getLocalProperty(propertyName: String): String {
    return getLocalProperty(propertyName, this)
}