package extensions

import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.kotlin.dsl.maven

/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:42.
 * ===================================================
 * Package              : extensions
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
/**
 * Adds all default repositories used to access to the different declared dependencies
 */
fun RepositoryHandler.applyDefault() {
    google()
    mavenCentral()
    maven("https://plugins.gradle.org/m2/")
}