package commons

import dependencies.Dependencies
import extensions.implementation

plugins {
    id ("org.jetbrains.kotlin.android")
}

dependencies {
    implementation(Dependencies.KOTLIN)
}