/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:50.
 * ===================================================
 * Package              :
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
/**
 * Configuration of product dimensions used on build variants
 */
object BuildProductDimensions {
    const val ENVIRONMENT = "environment"
}