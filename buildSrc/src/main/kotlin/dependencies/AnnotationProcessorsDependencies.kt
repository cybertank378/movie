package dependencies

/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:18.
 * ===================================================
 * Package              : depedencies
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */


/**
 * Project annotation processor dependencies, makes it easy to include external binaries or
 * other library modules to build.
 */
object AnnotationProcessorsDependencies {
    const val DAGGER = Dependencies.HILT
    const val ANDROID = Dependencies.HILT_ANDROID_KAPT
    const val GLIDE = Dependencies.GLIDE_KAPT

}