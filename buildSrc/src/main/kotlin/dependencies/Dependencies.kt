package dependencies

import BuildDependenciesVersions

/**
 * Created              : Rahman on 15/07/2022.
 * Date Created         : 15/07/2022 / 20:09.
 * ===================================================
 * Package              :
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
object Dependencies {

    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib:${BuildDependenciesVersions.kotlinVersion}"
    const val CORE_KTX = "androidx.core:core-ktx:${BuildDependenciesVersions.coreKtxVersion}"
    const val APPCOMPAT = "androidx.appcompat:appcompat:${BuildDependenciesVersions.appCompatVersion}"
    const val MATERIAL = "com.google.android.material:material:${BuildDependenciesVersions.materialVersion}"
    const val CONSTRAIT = "androidx.constraintlayout:constraintlayout:${BuildDependenciesVersions.constraintVersion}"
    const val RECYCLE = "androidx.recyclerview:recyclerview:${BuildDependenciesVersions.recyclerviewVersion}"
    const val WORK_MANAGER =  "androidx.work:work-runtime-ktx:${BuildDependenciesVersions.workVersion}"

    const val NAV_FRAGMENT = "androidx.navigation:navigation-fragment-ktx:${BuildDependenciesVersions.navVersion}"
    const val NAV_UI = "androidx.navigation:navigation-ui-ktx:${BuildDependenciesVersions.navVersion}"

    const val VIEWMODEL_KTX = "androidx.lifecycle:lifecycle-viewmodel-ktx:${BuildDependenciesVersions.lifecycleVersion}"
    const val LIVECYCLE_RUNTIME = "androidx.lifecycle:lifecycle-runtime-ktx:${BuildDependenciesVersions.lifecycleVersion}"
    const val LIVEDATA_KTX = "androidx.lifecycle:lifecycle-livedata-ktx:${BuildDependenciesVersions.lifecycleVersion}"


    const val HILT = "com.google.dagger:hilt-android:${BuildDependenciesVersions.hiltVersion}"
    const val HILT_VIEWMODEL = "androidx.hilt:hilt-lifecycle-viewmodel:${BuildDependenciesVersions.hiltLifecycleVersion}"
    const val HILT_ANDROID_KAPT =  "com.google.dagger:hilt-android-compiler:${BuildDependenciesVersions.hiltVersion}"
    const val HILT_KAPT = "androidx.hilt:hilt-compiler:${BuildDependenciesVersions.hiltCompilerVersion}"
    const val HILT_WORKER =  "androidx.hilt:hilt-work:${BuildDependenciesVersions.hiltWorkVersion}"


    const val COROUTINES_CORE = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${BuildDependenciesVersions.coroutinesVersion}"
    const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${BuildDependenciesVersions.coroutinesVersion}"


    const val GLIDE = "com.github.bumptech.glide:glide:${BuildDependenciesVersions.glideVersion}"
    const val GLIDE_KAPT = "com.github.bumptech.glide:compiler:${BuildDependenciesVersions.glideVersion}"

    const val TIMBER = "com.jakewharton.timber:timber:${BuildDependenciesVersions.timberVersion}"

    const val RETROFIT = "com.squareup.retrofit2:retrofit:${BuildDependenciesVersions.retrofitVersion}"
    const val GSON =  "com.squareup.retrofit2:converter-gson:${BuildDependenciesVersions.retrofitVersion}"
    const val LOGGING = "com.squareup.okhttp3:logging-interceptor:${BuildDependenciesVersions.okhttp3Version}"


    const val JUNIT =  "junit:junit:${BuildDependenciesVersions.junitVersion}"
    const val JUNIT_EXT = "androidx.test.ext:junit:${BuildDependenciesVersions.junitExtVersion}"
    const val ESPRESSO = "androidx.test.espresso:espresso-core:${BuildDependenciesVersions.espressoVersion}"


}