package dependencies

/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:27.
 * ===================================================
 * Package              : depedencies
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
/**
 * Project test android dependencies, makes it easy to include external binaries or
 * other library modules to build.
 */
object TestAndroidDependencies {
    const val ESPRESSO = Dependencies.ESPRESSO
    const val JUNIT_EXT =Dependencies.JUNIT_EXT
}