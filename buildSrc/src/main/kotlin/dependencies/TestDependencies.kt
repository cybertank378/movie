package dependencies

/**
 * Created              : Rahman on 16/07/2022.
 * Date Created         : 16/07/2022 / 00:30.
 * ===================================================
 * Package              : depedencies
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
/**
 * Project test dependencies, makes it easy to include external binaries or
 * other library modules to build.
 */
object TestDependencies {
    const val JUNIT = Dependencies.JUNIT
}