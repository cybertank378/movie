package io.rahman.moview

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

/**
 * Created              : Rahman on 15/07/2022.
 * Date Created         : 15/07/2022 / 18:30.
 * ===================================================
 * Package              : io.rahman.moview
 * Project Name         : Gibox-Moview.
 * Copyright            : Copyright @ 2022 cybertank378.
 */

@HiltAndroidApp
class MovieApp : Application(), Configuration.Provider {

    /**
     * Initial injected Hilt Worker
     */
    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    /**
     * @return The [Configuration] used to initialize WorkManager
     */
    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder()
            .setMinimumLoggingLevel(android.util.Log.INFO)
            .setWorkerFactory(workerFactory)
            .build()
    }
}